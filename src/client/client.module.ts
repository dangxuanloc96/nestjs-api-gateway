import { Module } from '@nestjs/common';
import {ClientController} from '../client/controllers/client.controller';
import { ClientService } from '../client/services/client.service';
import { TypeOrmModule } from '@nestjs/typeorm';
import {User, Chatbot, File} from 'entities/index'

@Module({
    imports: [TypeOrmModule.forFeature([User, Chatbot, File])], 
  controllers: [ClientController],
  providers: [ClientService]
})
export class ClientModule {}
