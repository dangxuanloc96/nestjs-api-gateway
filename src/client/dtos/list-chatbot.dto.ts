import { BaseDTO } from './base.dto';
import { ApiProperty } from '@nestjs/swagger';


class Chatbot {
    @ApiProperty({description: 'id chat bot'})
    readonly id: number;
  
    @ApiProperty({description: 'name chat bot',})
    readonly name: string;
  }

export class ListChatbotDTO extends BaseDTO {
    @ApiProperty({ type: [Chatbot] })
    readonly data: Chatbot [];
}


