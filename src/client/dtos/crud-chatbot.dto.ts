import { BaseDTO } from './base.dto';
import { ApiProperty } from '@nestjs/swagger';


class message {
    @ApiProperty({description: 'message'})
    readonly message: string;
  }

export class CRUDChatbotDTO extends BaseDTO {
    @ApiProperty({ type: message })
    readonly data: message;
}



