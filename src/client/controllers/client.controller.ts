import { Controller, Get, Query, Post, Put, Param, Delete, UseInterceptors} from '@nestjs/common';
import { FileInterceptor} from '@nestjs/platform-express';
import { Auth } from '@decorators/auth.decorator';
import { ApiTags, ApiBody, ApiConsumes} from '@nestjs/swagger';
import { ListChatbotDTO, CRUDChatbotDTO} from '../dtos';
import { ApiOkResponse, } from '@nestjs/swagger';
import { ApiFile } from '../daos';

@Auth()
@ApiTags('ClientService')
@Controller('client')
export class ClientController {
  constructor() {
  }

  @ApiOkResponse({ type: ListChatbotDTO })
  @Get('/chatbot')
  getInfo(@Query() query: string) {
    return [
        {
          "id": 1,
          "name": "Tư vấn ISO"
        },
        {
          "id": 1,
          "name": "Quy trình ISO"
        }
      ];
  }

  @ApiOkResponse({ type: CRUDChatbotDTO })
  @Post('/chatbot')
  @ApiConsumes('multipart/form-data')
  @ApiFile()
  @UseInterceptors(FileInterceptor('file'))
  createChatbot() {
    return {
        message: "create success"
    };
  }

  @ApiOkResponse({ type: CRUDChatbotDTO })
  @Put('/chatbot/:id')
  @ApiConsumes('multipart/form-data')
  @ApiFile()
  @UseInterceptors(FileInterceptor('file'))
  updateChatbot(@Param('id') id: number) {
    return {
        message: "update success"
    };
  }

  @ApiOkResponse({ type: CRUDChatbotDTO })
  @Delete('/chatbot/:id')
  deleteChatbot(@Param('id') id: number) {
    return {
        message: "delete success"
    };
  }
}
