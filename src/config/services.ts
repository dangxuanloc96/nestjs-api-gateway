import { Transport } from '@nestjs/microservices';

export const AUCTION_SERVICE = 'AUCTION_SERVICE';
export const ASSET_SERVICE = 'ASSET_SERVICE';

export interface IServiceConfig {
  name: string;
  transport: Transport;
  options: any;
}

export function auctionServiceConfig(): IServiceConfig {
  return {
    name: AUCTION_SERVICE,
    transport: Transport.TCP,
    options: {
      host: process.env.AUCTION_SVC_HOST || 'localhost',
      port: +process.env.AUCTION_SVC_PORT || 5001,
    },
  };
}

export function assetServiceConfig(): IServiceConfig {
  return {
    name: ASSET_SERVICE,
    transport: Transport.TCP,
    options: {
      host: process.env.ASSET_SVC_HOST || 'localhost',
      port: +process.env.ASSET_SVC_PORT || 5002,
    },
  };
}