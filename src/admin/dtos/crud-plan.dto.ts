import { BaseDTO } from './base.dto';
import { ApiProperty } from '@nestjs/swagger';


class Message {
    @ApiProperty({description: 'message'})
    readonly message: string;
  }

export class CRUDPlanDTO extends BaseDTO {
    @ApiProperty({ type: Message })
    readonly data: Message;
}



