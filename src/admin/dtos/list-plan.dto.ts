import { BaseDTO } from './base.dto';
import { ApiProperty } from '@nestjs/swagger';


class PlanModel {
    @ApiProperty({description: 'plan id'})
    readonly id: number;
  
    @ApiProperty({description: 'plan name',})
    readonly name: string;

    @ApiProperty({description: 'plan expired date',})
    readonly timeLimit: Date;

    @ApiProperty({description: 'plan bot minimum'})
    readonly botMinimum: number;

    @ApiProperty({description: 'plan daily token limit'})
    readonly dailyTokenLimit: number;
  }

  class Paginate {
    @ApiProperty()
    readonly totalPage: number;
  
    @ApiProperty()
    readonly page: number;

    @ApiProperty()
    readonly limit: number;
  }

  class DataPlan {
    @ApiProperty({ type: [PlanModel] })
    readonly data: PlanModel [];

    @ApiProperty({ type: Paginate })
    readonly paginate: Paginate;
  }

export class ListPlanDTO extends BaseDTO {
    @ApiProperty({ type: DataPlan })
    readonly data: DataPlan;
}


