import { BaseDTO } from './base.dto';
import { ApiProperty } from '@nestjs/swagger';

class Plan {
    @ApiProperty({description: 'plan name',})
    readonly name: string;

    @ApiProperty({description: 'plan status',})
    readonly status: number;

    @ApiProperty({description: 'plan expire date',})
    readonly expireDate: Date;

}


class Client {
    @ApiProperty({description: 'user id'})
    readonly id: number;
  
    @ApiProperty({description: 'user name',})
    readonly name: string;

    @ApiProperty({description: 'user email',})
    readonly email: string;

    @ApiProperty({description: 'user phone',})
    readonly phone: string;

    @ApiProperty({ type: [Plan] })
    readonly plans: Plan[];
  }

  class Paginate {
    @ApiProperty()
    readonly totalPage: number;
  
    @ApiProperty()
    readonly page: number;

    @ApiProperty()
    readonly limit: number;
  }

  class Data {
    @ApiProperty({ type: [Client] })
    readonly data: Client [];

    @ApiProperty({ type: Paginate })
    readonly paginate: Paginate;
  }

export class ListClientDTO extends BaseDTO {
    @ApiProperty({ type: Data })
    readonly data: Data;
}


