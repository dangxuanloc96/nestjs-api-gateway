import { Module } from '@nestjs/common';
import {ClientController} from '../admin/controllers/client.controller';
import {PlanController} from '../admin/controllers/plan.controller';
import { ClientService } from '../admin/services/client.service';
import { TypeOrmModule } from '@nestjs/typeorm';
import {User, Plan, UserPlan} from 'entities/index'

@Module({
    imports: [TypeOrmModule.forFeature([User, Plan, UserPlan])], 
  controllers: [ClientController, PlanController],
  providers: [ClientService]
})
export class AdminModule {}
