import { ApiProperty } from '@nestjs/swagger';


export class ListPlanDAO {
    @ApiProperty({ description: "key search", required: false})
    readonly keySearch: string;

    @ApiProperty({ description: "attribute sort", required: false, default: 'id'})
    readonly sort: string;

    @ApiProperty({ description: "DESC | ASC", required: false,  default: 'DESC'})
    readonly isDesc: string;

    @ApiProperty({ description: "page", required: false, default: 1})
    readonly page: number;

    @ApiProperty({ description: "perPage", required: false,  default: 20})
    readonly perPage: number;
}

