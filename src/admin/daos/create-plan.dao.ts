import { ApiProperty } from '@nestjs/swagger';


export class CreatePlanDAO {
    @ApiProperty({ description: "plan name"})
    readonly name: string;

    @ApiProperty({ description: "plan expire date"})
    readonly timeLimit: number;

    @ApiProperty({ description: "plan daily token limit"})
    readonly dailyTokenLimit: number;

    @ApiProperty({ description: "plan bot minimum"})
    readonly botMinimum: number;
}

