import { ApiProperty } from '@nestjs/swagger';


export class UpdateClientDAO {
    @ApiProperty({ description: "client status", required: false})
    readonly status: number;
}

