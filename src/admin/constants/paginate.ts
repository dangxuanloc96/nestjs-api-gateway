export const DEFAULT_PAGE = 1;
export const DEFAULT_PERPAGE = 20;
export const DEFAULT_ORDER_TYPE = "DESC";
export const DEFAULT_ORDER_KEY = "id";