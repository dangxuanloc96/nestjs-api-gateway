import { Controller, Get, Delete, Put, Query, Param, Body, Post } from '@nestjs/common';
import {ClientService} from '../services/client.service';
import { Auth } from '@decorators/auth.decorator';
import { ApiTags } from '@nestjs/swagger';
import { ApiOkResponse, } from '@nestjs/swagger';
import { ListPlanDTO, CRUDPlanDTO} from '../dtos';
import { ListPlanDAO, UpdatePlanDAO, CreatePlanDAO } from '../daos';

@Auth()
@ApiTags('AdminService')
@Controller('admin/plan')
export class PlanController {
  constructor(private readonly clientService: ClientService) {
  }

  @ApiOkResponse({ type: ListPlanDTO })
  @Get('/')
  listClient(@Query() query: ListPlanDAO) {
    return {
        "data": [
          {
            "id": 1,
            "name": "Free",
            "timeLimit": 1,
            "botMinimum": 1,
            "dailyTokenLimit": 1000 
          }
        ],
        "paginate": {
          "totalPage": 1,
          "page": 1,
          "limit": 20
        }
      }
  }

  @ApiOkResponse({ type: CRUDPlanDTO })
  @Post('/')
  createPlan(@Body() body: CreatePlanDAO) {
    return {
      message: "create success"
    }
  }

  @ApiOkResponse({ type: CRUDPlanDTO })
  @Put('/:id')
  updateClient(@Param('id') id: number, @Body() body: UpdatePlanDAO) {
    return {
      message: "update success"
    }
  }

  @ApiOkResponse({ type: CRUDPlanDTO })
  @Delete('/:id')
  deleteClient(@Param('id') id: number) {
    return {
      message: "delete success"
    }
  }

}
