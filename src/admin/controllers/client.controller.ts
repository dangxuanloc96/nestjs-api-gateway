import { Controller, Get, Delete, Put, Query, Param, Body } from '@nestjs/common';
import {ClientService} from '../services/client.service';
import { Auth } from '@decorators/auth.decorator';
import { ApiTags } from '@nestjs/swagger';
import { ApiOkResponse, } from '@nestjs/swagger';
import { ListClientDTO, CRUDClientDTO} from '../dtos';
import { ListClientDAO, UpdateClientDAO } from '../daos';

@Auth()
@ApiTags('AdminService')
@Controller('admin/client')
export class ClientController {
  constructor(private readonly clientService: ClientService) {
  }

  @ApiOkResponse({ type: ListClientDTO })
  @Get('/')
  listClient(@Query() query: ListClientDAO) {
    return this.clientService.list(query);
  }

  @ApiOkResponse({ type: CRUDClientDTO })
  @Put('/:id')
  updateClient(@Param('id') id: number, @Body() body: UpdateClientDAO) {
    return {
      message: "update success"
    }
  }

  @ApiOkResponse({ type: CRUDClientDTO })
  @Delete('/:id')
  deleteClient(@Param('id') id: number) {
    return {
      message: "delete success"
    }
  }

}
