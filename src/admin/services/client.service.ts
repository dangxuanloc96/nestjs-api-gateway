import { Injectable } from '@nestjs/common';
import { Repository } from 'typeorm';
import { InjectRepository } from '@nestjs/typeorm';
import { User} from 'entities/index';
import { BaseService } from './base.service';
@Injectable()
export class ClientService extends BaseService {
    constructor( @InjectRepository(User)
    private userRepository: Repository<User>,) {
       super();
    }

    async list(query: any) {
        const relationTable = ['plans'];
        const attributeSearch = ['name', 'email'];
        return this.paginate(this.userRepository, query, attributeSearch, relationTable);
    }
}
