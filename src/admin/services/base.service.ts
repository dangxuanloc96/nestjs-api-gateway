const { isPlainObject, isArray, mapKeys, camelCase } = require('lodash');
import { DEFAULT_PAGE, DEFAULT_PERPAGE, DEFAULT_ORDER_TYPE, DEFAULT_ORDER_KEY} from '../../admin/constants/paginate';
import { Like } from 'typeorm';

export abstract class BaseService {
    public convertToCamelCaseRecursive(
        data = null,
    )
    {
        if (isPlainObject(data)) {
            const convertedObject = {};
            Object.keys(data).forEach(key => {
              const value = data[key];
              const convertedKey = camelCase(key);
              convertedObject[convertedKey] = this.convertToCamelCaseRecursive(value);
            });
            return convertedObject;
          } else if (isArray(data)) {
            return data.map(item => this.convertToCamelCaseRecursive(item));
          }
          return data;
    }

    public async paginate(repository, query, attributes, relationsTables = [])
    {
      const totalRecord = (await repository.find()).length;
      
      const {keySearch, sort, isDesc, page, perPage} = query;
      const offset = !page || page <= 0 ? DEFAULT_PAGE : page;
      const limit = !perPage || perPage <= 0 ? DEFAULT_PERPAGE : perPage;

      const totalPage = totalRecord % limit == 0 ? Math.floor(totalRecord / limit) : Math.floor(totalRecord / limit) + 1 

      const attributeArrays = [];

      if(keySearch) {
        attributes.forEach(a => {
          attributeArrays.push({
            [a] : Like(`%${keySearch}%`)
          });
        });
      }

      const data = await repository.find({
        skip : (offset - 1) * limit,
        take : limit,
        relations: [...relationsTables],
        where : [
          ...attributeArrays
        ],
        order : {
          name: sort?? DEFAULT_ORDER_KEY,
          id: isDesc?? DEFAULT_ORDER_TYPE
        }
      });

      return {
        data,
        paginate: {
          totalPage,
          page: offset,
          limit
        }
      }
    }
}