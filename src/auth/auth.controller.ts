import { Body, Controller, Param, Post } from '@nestjs/common';
import { AuthService } from './auth.service';
import { LoginDAO, RegisterDAO } from './daos';
import { ApiTags } from '@nestjs/swagger';
import { ApiOkResponse, } from '@nestjs/swagger';
import { RegisterDTO} from './dtos/register.dto';
import { LoginDTO} from './dtos/login.dto';

@ApiTags('AuthService')
@Controller('auth')
export class AuthController {
  constructor(private readonly authService: AuthService) {}

  @Post('/login')
  @ApiOkResponse({ type: LoginDTO })
  login(@Body() infoUser: LoginDAO) {
    return this.authService.login(infoUser);
  }


  @Post('/register')
  @ApiOkResponse({ type: RegisterDTO })
  register(@Body() infoUser: RegisterDAO) {
    return this.authService.register(infoUser);
  }
}
