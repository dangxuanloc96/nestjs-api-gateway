import { Module } from '@nestjs/common';
import { AuthService } from './auth.service';
import { AuthController } from './auth.controller';
import { TypeOrmModule } from '@nestjs/typeorm';
import { JwtModule } from '@nestjs/jwt';
import {User} from 'entities/index'
import { Config } from '@common/config';
const JwtConfig = Config('jwt');
import { JwtStrategy } from './jwt.strategy';
import { PassportModule } from '@nestjs/passport';

@Module({
  imports: [TypeOrmModule.forFeature([User]), 
  JwtModule.register({
    secret: JwtConfig.secret,
    signOptions: { expiresIn: '1d' },
  }),
  PassportModule.register({
    defaultStrategy: 'jwt',
  }),
  ],
  controllers: [AuthController],
  providers: [AuthService, JwtStrategy],
})
export class AuthModule {}
