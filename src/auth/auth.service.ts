import { InjectRepository } from '@nestjs/typeorm';
import { MyLogger } from '@cores/logger';
import { Injectable, UnauthorizedException } from '@nestjs/common';
import { Repository } from 'typeorm';
import { LoginDAO } from './daos/login.dao';
import { User} from 'entities/index';
import { JwtService } from '@nestjs/jwt';
import * as bcrypt from 'bcryptjs';
import { RegisterDAO } from './daos';


@Injectable()
export class AuthService {
  logger = new MyLogger('AuthService');
  saltRounds = 10;
  constructor(
    @InjectRepository(User)
    private userRepository: Repository<User>,
    private readonly jwtService: JwtService
  ) {}

  async login(infoUser: LoginDAO) {
    const user = await this.userRepository.findOne({where: { email: infoUser.email }});
        if (user && (await bcrypt.compare(infoUser.password, user.password))) {
          const payload = { username: user.name, email: user.email };
          const token = this.jwtService.sign(payload);
          return {token: token}
        }
      throw new UnauthorizedException();
  }

  async register(infoUser: RegisterDAO) {
    return {
      "message": "register successs"
    }
  }
}
