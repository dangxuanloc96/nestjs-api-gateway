import { ApiProperty } from '@nestjs/swagger';
import { IsNotEmpty } from 'class-validator';

export class AddUserDAO {
  @ApiProperty({})
  @IsNotEmpty()
  readonly name: string;

  @ApiProperty({})
  @IsNotEmpty()
  readonly role: number;

  @ApiProperty({})
  @IsNotEmpty()
  readonly email: string;

  @ApiProperty({})
  @IsNotEmpty()
  readonly password: string;
}
