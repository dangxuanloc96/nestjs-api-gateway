import { ApiProperty } from '@nestjs/swagger';
import { IsNotEmpty } from 'class-validator';

export class LoginDAO {
  @ApiProperty({})
  @IsNotEmpty()
  readonly email: string;

  @ApiProperty({})
  @IsNotEmpty()
  readonly password: string;
}
