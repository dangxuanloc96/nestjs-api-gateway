import { ApiProperty } from '@nestjs/swagger';

export class BaseDTO {
  @ApiProperty()
  readonly statusCode: number;

  @ApiProperty()
  readonly message: string;

}
