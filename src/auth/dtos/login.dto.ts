import { BaseDTO } from './base.dto';
import { ApiProperty } from '@nestjs/swagger';


class Token {
    @ApiProperty({description: 'message'})
    readonly token: string;
  }

export class LoginDTO extends BaseDTO {
    @ApiProperty({ type: Token })
    readonly data: Token;
}



