import { BaseModel } from 'entities/base-model.entity';
import { Column, Entity, JoinTable, ManyToMany} from 'typeorm';
import  { User } from '../entities';

@Entity({ name: 'plans' })
export class Plan extends BaseModel {
  @Column({ length: 1000 })
  name: string;

  @Column()
  time_limit: number;

  @Column()
  daily_token_limit: number;

  @Column()
  bot_minimum: number;

  @ManyToMany(() => User, (user) => user.plans)
  @JoinTable({
    name: "user_plan", // table name for the junction table of this relation
    joinColumn: {
        name: "plan_id",
        referencedColumnName: "id"
    },
    inverseJoinColumn: {
        name: "user_id",
        referencedColumnName: "id"
    }
})
users: User[]
}

