import { BaseModel } from 'entities/base-model.entity';
import { Column, Entity, JoinTable, ManyToMany } from 'typeorm';
import { Plan } from '../entities/index';
import { Exclude } from 'class-transformer';

@Entity({ name: 'users' })
export class User extends BaseModel {
  @Column({ length: 1000 })
  name: string;

  @Column({ length: 1000 })
  email: string;

  @Column({ length: 1000 })
  phone: string;

  @Column()
  role: number;

  @Column({ length: 1000 })
  @Exclude({})
  password: string;

  @ManyToMany(() => Plan, (plan) => plan.users)
  @JoinTable({
    name: "user_plan", // table name for the junction table of this relation
    joinColumn: {
        name: "user_id",
        referencedColumnName: "id"
    },
    inverseJoinColumn: {
        name: "plan_id",
        referencedColumnName: "id"
    }
})
  plans: Plan[]
}

