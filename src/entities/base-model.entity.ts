import { PrimaryGeneratedColumn } from 'typeorm';
import { BaseTimestampModel } from './base-timestamp-model.entity';

export abstract class BaseModel extends BaseTimestampModel {
  @PrimaryGeneratedColumn()
  id: number;
}

