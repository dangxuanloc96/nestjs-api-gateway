import { BaseModel } from 'entities/base-model.entity';
import { Column, Entity} from 'typeorm';

@Entity({ name: 'files' })
export class File extends BaseModel {
  @Column()
  chatbot_id: number;
  
  @Column({ length: 1000 })
  name: string;

  @Column({ length: 1000 })
  path: string;
}

