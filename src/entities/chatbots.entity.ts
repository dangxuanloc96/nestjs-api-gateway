import { BaseModel } from 'entities/base-model.entity';
import { Column, Entity} from 'typeorm';

@Entity({ name: 'chatbots' })
export class Chatbot extends BaseModel {
  @Column()
  user_id: number;
  
  @Column({ length: 1000 })
  name: string;
}

