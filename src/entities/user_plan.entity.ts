import { BaseModel } from 'entities/base-model.entity';
import { Column, Entity, Timestamp } from 'typeorm';

@Entity({ name: 'user_plan' })
export class UserPlan extends BaseModel {
  @Column()
  user_id: number;

  @Column()
  plan_id: number;

  // @Column()
  // start_date: Date;

  // @Column()
  // end_date: Date;

  @Column()
  status: number;
}

