import { ConsoleLogger } from '@nestjs/common';

export class MyLogger extends ConsoleLogger {
  static throwError(exception) {
    throw exception;
  }
}
