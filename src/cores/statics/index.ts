import * as fs from 'fs';
import * as path from 'path';

const STATIC_DIR = 'statics';

export function LoadStatic(fileName: string) {
  const projectDir = path.resolve('./');
  const filePath = path.join(projectDir, `${STATIC_DIR}/${fileName}`);
  return fs.readFileSync(filePath, 'utf8');
}
