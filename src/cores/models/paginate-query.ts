import { ApiProperty } from '@nestjs/swagger';
import { IsOptional } from 'class-validator';

export class PaginateQuery {
  @ApiProperty({ default: 1, required: false })
  @IsOptional()
  readonly page: number;

  @ApiProperty({ default: 20, required: false })
  @IsOptional()
  readonly per_page: number;

  @ApiProperty({ required: false })
  @IsOptional()
  readonly search: string;
}
