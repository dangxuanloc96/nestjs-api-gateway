import { ApiProperty } from '@nestjs/swagger';
import { IsNotEmpty } from 'class-validator';

export class Pagination<T> {
  @ApiProperty()
  @IsNotEmpty()
  readonly count: number;

  @ApiProperty({ default: 1 })
  @IsNotEmpty()
  readonly page: number;

  @ApiProperty({ default: 20 })
  @IsNotEmpty()
  readonly per_page: number;

  @ApiProperty()
  @IsNotEmpty()
  readonly total_page: number;

  @ApiProperty()
  @IsNotEmpty()
  readonly records: T[];
}
