import { HttpException, HttpStatus } from '@nestjs/common';

const Emote = '(ノಠ益ಠ)ノ彡┻━┻';

export class BadRequest extends HttpException {
  constructor(message) {
    const msg = `${message} ${Emote}`;
    super(msg, HttpStatus.BAD_REQUEST);
    console.log(this.stack);
  }
}

export class InternalServerError extends HttpException {
  constructor(message) {
    const msg = `${message} ${Emote}`;
    super(msg, HttpStatus.INTERNAL_SERVER_ERROR);
    console.log(this.stack);
  }
}

export class Forbidden extends HttpException {
  constructor(message) {
    const msg = `${message} ${Emote}`;
    super(msg, HttpStatus.FORBIDDEN);
    console.log(this.stack);
  }
}

export class NotFound extends HttpException {
  constructor(message) {
    const msg = `${message} ${Emote}`;
    super(msg, HttpStatus.NOT_FOUND);

    console.log(this.stack);
  }
}
