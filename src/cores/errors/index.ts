export * from './exceptions';

export enum ErrorCode {
  INVALID_PARAMETERS = 10000,
  CALL_SERVICE_FAILED = 503,
  AUTHORIZE = 401,
}
