import {
  ClassSerializerInterceptor,
  Module,
  ValidationPipe,
} from '@nestjs/common';
import { AppController } from './app.controller';
import { AppService } from './app.service';
import { LoggerService } from './logger/logger.service';
import { ConfigModule } from '@nestjs/config';
import { TransformInterceptor } from '@cores/interceptors';
import { APP_INTERCEPTOR, APP_PIPE, APP_FILTER} from '@nestjs/core';
import {ChatModule} from './proxy/chat/chat.module';
import { AuthModule } from './auth/auth.module';
import { TypeOrmModule } from '@nestjs/typeorm';
import {User, Plan, UserPlan} from 'entities';
import { HttpExceptionFilter } from '@cores/filters/http-exception.filter';
import {AdminModule} from './admin/admin.module';
import {ClientModule} from './client/client.module';

@Module({
  //comment AssetModule, AuctionModule, EventsModule
  imports: [ConfigModule.forRoot(), AuthModule, ChatModule, AdminModule, ClientModule,
    TypeOrmModule.forRoot({
      type: 'mysql',
      host: 'localhost',
      port: 3306,
      username: 'root',
      password: '',
      database: 'chatgpt',
      entities: [User, Plan, UserPlan],
      synchronize: false,
      charset: 'utf8',
      logging: true
    }),
  ],
  controllers: [AppController],
  providers: [
    AppService,
    LoggerService,
    {
      provide: APP_FILTER,
      useClass: HttpExceptionFilter,
    },
    {
      provide: APP_INTERCEPTOR,
      useClass: TransformInterceptor,
    },
    {
      provide: APP_INTERCEPTOR,
      useClass: ClassSerializerInterceptor,
    },
    {
      provide: APP_PIPE,
      useValue: new ValidationPipe({ whitelist: true, transform: true }),
    }
  ],
})
export class AppModule {}
