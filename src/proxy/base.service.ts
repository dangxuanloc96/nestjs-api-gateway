import { Injectable } from '@nestjs/common';

@Injectable()
export abstract class BaseService {
  protected logger;
  protected config;

  constructor() {}

  protected getHeaders() {
    return this.config['headers'];
  }

  protected getUrl(endpointKey: string) {
    const domain = this.config['domain'];
    const endpoint = this.config['endpoints'][endpointKey];
    return domain + endpoint;
  }
}
