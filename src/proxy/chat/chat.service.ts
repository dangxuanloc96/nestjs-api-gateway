import { Injectable } from '@nestjs/common';
import {BaseService} from '../base.service';
import { CustomHttpService as HttpService } from '../http.service';

import { Config } from '@common/config';
import { MyLogger } from '@common/logger';

@Injectable()
export class ChatService extends BaseService {
    constructor(private HttpService: HttpService) {
        super();
        this.logger = new MyLogger('test');
        this.config = Config('test');
    }

    public async getInfo(q: string, file: string) {
        const url = this.getUrl('chat');
        const result = await this.HttpService.get(url + `?q=${q}&file=${file}`, {
            headers: this.getHeaders(),
          });
          return result.data;
    }
  
}
