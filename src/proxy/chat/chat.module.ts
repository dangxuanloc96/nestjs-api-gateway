import { Module } from '@nestjs/common';
import {ChatController} from './chat.controller';
import { HttpModule } from '@nestjs/axios';
import { ChatService } from './chat.service';
import { CustomHttpService as HttpService } from '../http.service';

@Module({
    imports: [HttpModule], 
  controllers: [ChatController],
  providers: [HttpService, ChatService]
})
export class ChatModule {}
