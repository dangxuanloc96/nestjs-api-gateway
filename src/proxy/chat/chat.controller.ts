import { Controller, Get, Query } from '@nestjs/common';
import {ChatService} from './chat.service';
import { Auth } from '@decorators/auth.decorator';
import { ApiTags } from '@nestjs/swagger';

@Auth()
@ApiTags('ChatService')
@Controller('chat')
export class ChatController {
  constructor(private readonly chatService: ChatService) {}

  @Get('/')
  getInfo(@Query('q') q: string, @Query('file') file: string) {
    return this.chatService.getInfo(q, file);
  }
}
