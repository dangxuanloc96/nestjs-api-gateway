import { Controller, Get, Catch, UnauthorizedException  } from '@nestjs/common';
import { AppService } from './app.service';
import {Auth} from '@decorators/auth.decorator';
import { MyLogger } from '@cores/logger';

@Auth()
@Catch(UnauthorizedException)
@Controller()
export class AppController {
  logger = new MyLogger('Test');
  constructor(private readonly appService: AppService) {}

  @Get()
  getHello(): string {
    try {
      return this.appService.getHello();
    } catch(e) {
      this.logger.error(e);
      return "unauthorize";
    }
  }
}
