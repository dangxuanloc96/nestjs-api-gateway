import { NestFactory } from '@nestjs/core';
import { AppModule } from './app.module';
import { MyLogger } from '@cores/logger/my-logger';
import * as packageConfig from './../package.json';
import { SwaggerModule, DocumentBuilder } from '@nestjs/swagger';
import { BASE_API_PREFIX } from '@config/main-config';
import { HttpExceptionFilter } from '@cores/filters/http-exception.filter';
import { INestApplication } from '@nestjs/common';
import { NestExpressApplication } from '@nestjs/platform-express';

function buildApiDoc(app: INestApplication, urlPath = 'apidoc') {
  const config = new DocumentBuilder()
    .setTitle(packageConfig.name)
    .setDescription(packageConfig.description)
    .setVersion(packageConfig.version)
    .addServer(`${process.env.BASE_URL}${BASE_API_PREFIX}`)
    .addBearerAuth()
    .build();
  const document = SwaggerModule.createDocument(app, config);
  SwaggerModule.setup(urlPath, app, document);
  console.log('Create Swagger Docs with path:', urlPath);
}

async function bootstrap() {
  const app = await NestFactory.create<NestExpressApplication>(AppModule, {
    logger: new MyLogger(),
  });
  const port = process.env.PORT || 5000;

  if (process.env.NODE_ENV !== 'production') {
    buildApiDoc(app);
  }
  app.enableCors();
  console.log(`Application start at port: ${port}`);
  app.useGlobalFilters(new HttpExceptionFilter());
  await app.listen(port);
}

bootstrap();
