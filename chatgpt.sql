/*
 Navicat Premium Data Transfer

 Source Server         : test
 Source Server Type    : MySQL
 Source Server Version : 50733
 Source Host           : localhost:3306
 Source Schema         : chatgpt

 Target Server Type    : MySQL
 Target Server Version : 50733
 File Encoding         : 65001

 Date: 26/06/2023 17:40:49
*/

SET NAMES utf8mb4;
SET FOREIGN_KEY_CHECKS = 0;

-- ----------------------------
-- Table structure for users
-- ----------------------------
DROP TABLE IF EXISTS `users`;
CREATE TABLE `users`  (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `created_at` timestamp(6) NOT NULL DEFAULT CURRENT_TIMESTAMP(6),
  `updated_at` timestamp(6) NOT NULL DEFAULT CURRENT_TIMESTAMP(6) ON UPDATE CURRENT_TIMESTAMP(6),
  `deleted_at` datetime(6) NULL DEFAULT NULL,
  `name` varchar(1000) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL,
  `email` varchar(1000) CHARACTER SET latin1 COLLATE latin1_swedish_ci NOT NULL,
  `password` varchar(1000) CHARACTER SET latin1 COLLATE latin1_swedish_ci NOT NULL,
  `role` int(11) NOT NULL,
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 4 CHARACTER SET = latin1 COLLATE = latin1_swedish_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of users
-- ----------------------------
INSERT INTO `users` VALUES (1, '2023-06-22 16:21:47.000000', '2023-06-26 14:11:30.302438', NULL, 'Đăng Xuân Lộc', 'dangxuanloc96@gmail.com', '$2a$12$teqaKULLdge2XgWfisKQLOkhzxGZ/qgLmzcq5FwJMxntEvE.zTDiS', 0);
INSERT INTO `users` VALUES (2, '2023-06-26 14:45:47.829439', '2023-06-26 14:45:47.829439', NULL, 'Nguyên Huy Hoàng', 'huyhoangit.jvb@gmail.com', '$2a$10$5/k0M9fb2n77UgbDXK4yVuO//tbaa8hu3HsIdZmzJ.kEWZksPciwq', 1);
INSERT INTO `users` VALUES (3, '2023-06-26 15:18:35.429396', '2023-06-26 15:18:35.429396', NULL, 'Nguyên Huy Mẫn', 'huyhoangit1.jvb@gmail.com', '$2a$10$HGB9o/RwYaVgH/3Two.BTuAz54OdB9c7vg8rQawfhDOmfXCvX5OL2', 1);

SET FOREIGN_KEY_CHECKS = 1;
