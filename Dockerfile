FROM trinhnv/node-git:14

WORKDIR /usr/src/app

COPY package.json yarn.lock ./

RUN yarn ci

EXPOSE 3000

COPY . .

CMD ["yarn", "start"]